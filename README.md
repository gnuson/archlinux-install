This script will guide for installing Arch Linux from the live system booted with the official installation image.
<p>     * 1 Pre-installation</p>
<p>          + 1.1 Set the keyboard layout</p>
<p>          + 1.2 Connect to the Internet</p>
<p>          + 1.3 Update the system clock</p>
<p>          + 1.4 Partition the disks</p>
<p>          + 1.5 Format the partitions</p>
<p>          + 1.6 Mount the partitions</p>
<p>     * 2 Installation</p>
<p>          + 2.1 Select the mirrors</p>
<p>          + 2.2 Install the base packages</p>
<p>          + 2.3 Configure the system</p>
<p>          + 2.4 Install a boot loader</p>
<p>          + 2.5 Reboot</p>
<p>     * 3 Post-installation </p>